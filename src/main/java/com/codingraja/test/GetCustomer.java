package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Address;
import com.codingraja.domain.Customer;

public class GetCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("config/hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		/*This is bidirectional one-to-one association, so if you
		have Customer ID then you can fetch Customer Record as well as 
		Address of Customer*/
		
		System.out.println("--Fetching Customer and Address using Customer ID--");		
		Session session = factory.openSession();
		Customer customer = session.get(Customer.class, new Long(1));
		Address address = customer.getAddress();
		session.close();
		
		System.out.println("Customer Email: "+customer.getEmail());
		System.out.println("Customer City: "+address.getCity());
		
		/*if you have Address ID then you can fetch Customer Address and 
		Customer Record, because this is bidirectional .*/
		
		System.out.println("--Fetching Customer and Address using Address ID--");
		Session session1 = factory.openSession();
		Address address1 = session1.get(Address.class, new Long(1));
		Customer customer1 = address1.getCustomer();
		session1.close();
		
		System.out.println("Customer Mobile: "+customer1.getMobile());
		System.out.println("Zipcode: "+address1.getZipcode());
		
	}
}
